from django.db import models

from coffee_shop_app.models import Addition
from coffee_shop_app.models import Beverage
from coffee_shop_app.models import Order


class CompletedBeverage(models.Model):
    beverage = models.ForeignKey(Beverage, on_delete=models.SET_NULL, null=True)
    additions = models.ManyToManyField(Addition)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, related_name='completed_beverages', null=True)

    def get_price(self):
        price = self.beverage.price
        for addition in self.additions.all():
            price += addition.price
        return price
