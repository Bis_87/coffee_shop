from django.db import models

from coffee_shop_app.models import CoffeeUser

import time


class Order(models.Model):
    salesman = models.ForeignKey(CoffeeUser, on_delete=models.SET_NULL, null=True)
    total = models.FloatField(max_length=10, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    def get_price(self):
        price = 0
        for beverage in self.completed_beverages.all():
            price += beverage.get_price()
        return price

    @staticmethod
    def get_additions_text(additions_qs):
        """

        :param additions_qs: queryset of additions
        :return: text representation for bill
        """
        if additions_qs.exists():
            a = ''
            for addition in additions_qs:
                a += '%s : %s$,  ' % (addition.name, addition.price)
            return a
        return ''

    def save_bill(self, user, order):
        """
        creates file with order info
        :param user: logged in user 
        :param order: order that will be saved
        :return: 
        """
        filename = str(user.id) + '_' + time.strftime("%b_%d_%Y__%H:%M:%S")

        completed_beverages_qs = order.completed_beverages

        beverage_details = {}
        for beverage_number, i in enumerate(completed_beverages_qs.all()):
            beverage_details['beverage_' + str(beverage_number + 1)] = dict()
            beverage_details['beverage_' + str(beverage_number + 1)]['coffee'] = i.beverage.name
            beverage_details['beverage_' + str(beverage_number + 1)]['coffee_price'] = i.beverage.price
            beverage_details['beverage_' + str(beverage_number + 1)]['additions'] = self.get_additions_text(
                i.additions.all())

        print(beverage_details)

        info = {
            'User name': user.username,
            'User id': user.id,
            'Order ID': order.id,
            'Total': order.total,
            'Date': order.created_at.strftime("%b.%d.%Y, %H:%M:%S"), }

        with open('./bills/%s' % filename, 'w') as f:
            for key in info:
                f.write(key + ' : ' + str(info[key]) + '\n')
            for key in beverage_details:
                f.write(key + ' : ' + str(beverage_details[key]) + '\n')

    def get_additions(self):
        additions = []
        for addition in self.completed_beverages.all():
            price = addition.p
