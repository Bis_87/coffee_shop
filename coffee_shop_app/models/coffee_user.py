from django.db import models

from coffee_shop_app.name_validator import validate_name


class CoffeeUser(models.Model):
    username = models.CharField(max_length=50, unique=True, validators=[validate_name, ])
    USER_POSITION_SALESMAN = 'SALESMAN'
    USER_POSITION_MANAGER = 'MANAGER'
    USER_POSITION_CHOICES = (
        (USER_POSITION_MANAGER, 'Manager'),
        (USER_POSITION_SALESMAN, 'Salesman'),
    )
    user_position = models.CharField(max_length=10,
                                     choices=USER_POSITION_CHOICES, )

    creation_date = models.DateTimeField(auto_now_add=True)

    def get_position(self):
        return self.user_position

    def __str__(self):
        return '%s %s' % (self.user_position, self.username)
