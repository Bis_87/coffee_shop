from django.db import models

from coffee_shop_app.name_validator import validate_name


class Addition(models.Model):
    name = models.CharField(
        max_length=50,
        unique=True,
        validators=[validate_name, ]
    )

    price = models.FloatField(
        max_length=10,
    )

    def __str__(self):
        return self.name
