from coffee_shop_app.models import CoffeeUser


def get_user(name, role):
    if not name:
        name = input('Type your username: ')

    user_qs = CoffeeUser.objects.filter(username=name)

    if not user_qs.exists():
        print('User %s not found' % name)
        return get_user(None, role)
    user = user_qs.first()

    if user.user_position == role:
        return user

    print('\nUser %s is not a %s\n' % (name, role) )
    exit(1)