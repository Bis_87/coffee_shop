from django.apps import AppConfig


class CoffeeShopAppConfig(AppConfig):
    name = 'coffee_shop_app'
