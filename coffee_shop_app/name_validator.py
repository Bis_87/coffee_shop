import re

def validate_name(value):
    return bool(re.match("^[0-9a-zA-Z _]+$", value))
