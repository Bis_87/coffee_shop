from django.core.management import BaseCommand
from coffee_shop_app.models import Beverage
from six.moves import input
from coffee_shop_app.name_validator import validate_name

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--name', help='Type name of new beverage')
        parser.add_argument('--price', help='Type price')

    def get_beverage_name(self, beverage_name=None):
        if not beverage_name:
            beverage_name = input('Enter beverage name\n')
        if not validate_name(beverage_name) or Beverage.objects.filter(name=beverage_name).exists():
            print('Beverage name is invalid')
            return self.get_beverage_name()
        return beverage_name

    def get_price(self, price=None):
        if not price:
            price = input('Enter price: \n')

        try:
            float(price)
        except:
            print('price is invalid')
            return self.get_price()

        return price

    def handle(self, *args, **options):
        b_name = self.get_beverage_name(options.get('name'))
        b_price = self.get_price(options.get('price'))
        Beverage.objects.create(name=b_name, price=b_price)
        print('Beverage created')