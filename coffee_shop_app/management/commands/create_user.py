from django.core.management import BaseCommand
from coffee_shop_app.models import CoffeeUser
from six.moves import input


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--user', help='Type username of new user')
        parser.add_argument('--role', help='Type role of new user')

    def get_role(self, role_name=None):
        if not role_name:
            role_name = input('Enter user position: \n Choices: \n  SALESMAN \n  MANAGER\n')

        if role_name not in {CoffeeUser.USER_POSITION_MANAGER, CoffeeUser.USER_POSITION_SALESMAN}:
            return self.get_role()
        return role_name

    def get_username(self, name=None):
        if not name:
            name = input('Enter username: \n')
        if CoffeeUser.objects.filter(username=name).exists():
            print('username is already exists\n')
            return self.get_username()
        return name

    def handle(self, *args, **options):
        username = self.get_username(options.get('user'))
        role = self.get_role(options.get('role'))
        CoffeeUser.objects.create(
            username=username,
            user_position=role
        )
        print('User created')

