from django.core.management.base import BaseCommand
from six.moves import input

from coffee_shop_app.models import Order, CoffeeUser, CompletedBeverage, Beverage, Addition
from coffee_shop_app.services.user_indentification import get_user
from coffee_shop_app.validator import OptionsValidator


class Command(BaseCommand):
    help = 'interface to be used by salesmans'

    def add_arguments(self, parser):
        """
        Entry point for subclassed commands to add custom arguments.
        :param parser: parser for arguments
        :return: None
        """
        parser.add_argument('--user', required=True, help='Enter your username. This is required.')
        parser.add_argument('--prices',
                            help='Type "beverages" or "additions" to get prices of desirable product groups. Type "all" to get all prices')
        parser.add_argument('--beverage', help='Enter existing beverage name.')
        parser.add_argument('--additions', help='Enter existing addition name.')

    def add_additions(self, completed_beverage, additions):
        """
        :param completed_beverage: object with already added beverage
        :param additions: used to get additions from command line argument
        :return: 
        """
        # if additions absent in command line
        if not additions:
            print('AVAILABLE ADDITIONS:')
            additions_qs = Addition.objects.all()

            for addition in additions_qs:
                print(addition.name, '%s$' % addition.price)
            additions = input('Enter comma-separated additions: \nEnter blank line if additions is not needed: ')

            # validate passed addition - if it is exists
            if not OptionsValidator.is_valid_additions(additions):
                print('Additions are invalid')
                return self.add_additions(completed_beverage, None)
        # empty string means user added all additions
        if additions == '':
            return
        # parse additions added by user via commandline input
        additions_list = additions \
            .replace(' ', '') \
            .split(',')
        for addition in additions_list:
            completed_beverage.additions.add(Addition.objects.get(name=addition))

    def get_beverage(self, coffee_name):
        """
       
        :param coffee_name: name of beverage
        :param already_exists: flag is at least one beverage exists in order
        :return: beverage object
        """
        # if beverage name is not passed through commandline prints available beverages
        # and ask user to type beverage name
        if not coffee_name:
            coffee_qs = Beverage.objects.all()
            print('AVAILABLE COFFEE:')

            for coffee in coffee_qs:
                print(coffee.name, '%s$' % coffee.price)

            coffee_name = input(' \nEnter blank line if coffee is not needed: \nEnter coffee name: ')

            if coffee_name == '' and self.order.completed_beverages.exists():
                return None
        # validates beverage name - if it is exists
        if not OptionsValidator.is_valid_beverage(coffee_name):
            print('Coffee not found')
            return self.get_beverage(None)

        return Beverage.objects.filter(name=coffee_name).first()

    def add_completed_beverage(self, options):
        """
        
        :param options: creates completed beverage object to be added to order
        :param already_exists: flag to check if there is already added beverage
        :return: 
        """
        beverage = self.get_beverage(options.pop('beverage', None))

        if beverage:
            c = CompletedBeverage.objects.create(
                order=self.order,
                beverage=beverage
            )
            self.add_additions(c, options.get('additions'))

            options.pop('additions', None)

            self.add_completed_beverage(options)

    def get_prices(self, options):
        """
        
        :param options: commandline args dict  
        :return: print prices into commandline
        """
        beverages_qs = Beverage.objects.all()
        additions_qs = Addition.objects.all()
        if options.get('prices') == 'all':
            print('BEVERAGES')
            for beverage in beverages_qs:
                print(beverage.name, '%s$' % beverage.price)
            print('ADDITIONS')
            for addition in additions_qs:
                print(addition.name, '%s$' % addition.price)
        elif options.get('prices') == 'beverages':
            print('BEVERAGES')
            for beverage in beverages_qs:
                print(beverage.name, '%s$' % beverage.price)
        elif options.get('prices') == 'additions':
            print('ADDITIONS')
            for addition in additions_qs:
                print(addition.name, '%s$' % addition.price)

    def handle(self, *args, **options):
        """
        The actual logic of the command. Subclasses must implement this method.
        :param args: 
        :param options: 
        :return: 
        """
        validator = OptionsValidator(options)
        if not validator.is_valid():
            print('Data is invalid')
            exit(1)

        user = get_user(options.get('user'), CoffeeUser.USER_POSITION_SALESMAN)

        if options.get('prices'):
            self.get_prices(options)
            exit(0)

        self.order = Order.objects.create(salesman=user)
        self.add_completed_beverage(options)

        order_sum = self.order.get_price()
        self.order.total = order_sum

        self.order.save_bill(user, self.order)

        self.order.save()

        print('Order created')
        print('Order total: %s $' % (order_sum))
