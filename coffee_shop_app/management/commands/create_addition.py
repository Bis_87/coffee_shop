from django.core.management import BaseCommand
from coffee_shop_app.models import Addition
from six.moves import input
from coffee_shop_app.name_validator import validate_name

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--name', help='Type name of new Addition')
        parser.add_argument('--price', help='Type price')

    def get_addition_name(self, addition_name=None):
        if not addition_name:
            addition_name = input('Enter addition name\n')
        if not validate_name(addition_name) or Addition.objects.filter(name=addition_name).exists():
            print('Addition name is invalid')
            return self.get_addition_name()
        return addition_name

    def get_price(self, price=None):
        if not price:
            price = input('Enter price: \n')

        try:
            float(price)
        except:
            print('price is invalid')
            return self.get_price()

        return price

    def handle(self, *args, **options):
        b_name = self.get_addition_name(options.get('name'))
        b_price = self.get_price(options.get('price'))
        Addition.objects.create(name=b_name, price=b_price)
        print('Addition created')