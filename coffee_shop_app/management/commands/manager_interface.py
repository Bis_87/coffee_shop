from django.core.management.base import BaseCommand
from terminaltables import AsciiTable

from coffee_shop_app.models import CoffeeUser
# Order.objects.filter(salesman_id=2).count()
from coffee_shop_app.services.user_indentification import get_user


class Command(BaseCommand):
    help = 'Used to get sales result for each salesman'

    def add_arguments(self, parser):
        parser.add_argument('--user', required=True, help='Enter your username. This is required.')

    def get_all_salesmans_data(self):
        """
        
        :return: list with data
        """

        data_list = []
        users = CoffeeUser.objects.filter(user_position='SALESMAN')

        for user in users:
            user_sum = 0
            user_list = []

            user_list.append(user.id)
            user_list.append(user.username)
            orders_qs = user.order_set.all()
            for order in orders_qs:
                user_sum += order.get_price()
            user_list.append(orders_qs.count())
            user_list.append(user_sum)
            data_list.append(user_list)

        return (data_list)

    def handle(self, *args, **options):

        get_user(options.get('user'), CoffeeUser.USER_POSITION_MANAGER)

        table_data = [['Seller id', 'Seller name', 'Number of sales', 'Total Value ($)'], ]
        salesmans_data = self.get_all_salesmans_data()

        for data in salesmans_data:
            table_data.append(data)
        table = AsciiTable(table_data)
        print(table.table)
