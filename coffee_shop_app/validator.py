from coffee_shop_app.models import Beverage, Addition

class OptionsValidator(object):
    def __init__(self, options):
        self.beverage = options.get('beverage')
        self.additions = options.get('additions')

    @staticmethod
    def is_valid_beverage(item):
        if not item:
            return True
        return Beverage.objects.filter(name=item).exists()

    @staticmethod
    def is_valid_additions(additions):
        if not additions:
            return True
        for i in additions.split(','):
            if not Addition.objects.filter(name=i.strip()).exists():
                return False
        return True


    def is_valid(self):
        return self.is_valid_beverage(self.beverage) and \
               self.is_valid_additions(self.additions)

