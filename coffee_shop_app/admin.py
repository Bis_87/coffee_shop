from django.contrib import admin

from .models import (
    CoffeeUser,
    Beverage,
    CompletedBeverage,
    Addition,
    Order
)


admin.site.register(CoffeeUser)
admin.site.register(Beverage)
admin.site.register(CompletedBeverage)
admin.site.register(Addition)
admin.site.register(Order)

#
# Register your models here.
