## Coffee for you commandline application

Based on Django 1.11

Python 2.7 and 3.* supported

Dependencies are in requirements.txt


**Create environment**

Create python virtual environment

    python3 -m venv /path/to/new/virtual/environment
    
venv manual: <https://docs.python.org/3/library/venv.html>

    
install dependencies from requirements requirements.txt:

    pip install -r requirements.txt
    
create db, make migrations, create admin:

    python manage.py migrate
    python manage.py createsuperuser
    

**How to use**

To get access to django admin:
    
    python manage.py runserver

Django admin panel will be available at:
 
    http://127.0.0.1:8000/admin
    
To login enter your superuser credentials


To get help message:
    
    python manage.py <your_interface> --help


For Salesman role:

    python manage.py salesman_interface <args>
    
    required arg:   --username <your username>
    
    optional args:  --beverage <choosen_beverage>
                    --additions <add_1,add_2,add_3>
                    --prices <all> OR <additions> OR <beverages> 
                    
For Manager role:  
                  
    python manage.py manager_interface <args>
    
    required arg:   --username <your username>
    
To create new user:

    python manage.py create_user <args>
    
    --user Type username of new user
    --role Type role of new user
    
To create new beverage:

    python manage.py create_beverage <args>
    
    --name  Type name of new beverage
    --price Type price
    
To create new addition:

    python manage.py create_beverage <args>
    
    --name  Type name of new Addition
    --price Type price